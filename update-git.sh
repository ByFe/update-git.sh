#!/bin/bash -

GITVER=0.0.0
GITVER_INSTALLED=0.0.0
DURL=https://www.kernel.org/pub/software/scm/git
RURL=https://www.kernel.org/pub/scm/git/git.git
DDIR=~/Downloads
IPREF=/usr/local
VPAT='([0-9]+\.[0-9]+\.[0-9]+)(\.[0-9]+)?'
CWD=$(pwd)

# required packages: curl, zlib, openssl, expat, libiconv
# ubuntu: libcurl4-gnutls-dev libcurl4-openssl-dev zlib1g-dev libssl-dev libexpat1-dev gettext

function isValidVersionNumber {
	if [ ! -z $1 ]; then
		if [[ "$1" =~ ^${VPAT}$ ]]; then
			return 0
		fi
	fi
	return 1
}

function getLatestStableGitRelease {
	TMPVER=$(git ls-remote --tags $RURL | sed 's@.*refs/tags/v\(.*\)\^{}@\1@' | egrep "^${VPAT}$" | sort -r | head -n 1)
	isValidVersionNumber $TMPVER || TMPVER=0.0.0
	echo $TMPVER
}

function getInstalledGitVersion {
	TMPVER=$(git version | awk -v pat='$VPAT' 'pat {print $NF}')
	isValidVersionNumber $TMPVER || TMPVER=0.0.0
	echo $TMPVER
}

function promptForVersion {

	while true; do
		read -p "# which version should be installed? : " GITVER
		isValidVersionNumber $GITVER && break
		echo "# invalid version number format (should be  x.x.x[.x])"
	done
}

GITVER=$(getLatestStableGitRelease)
GITVER_INSTALLED=$(getInstalledGitVersion)

if  [[ $GITVER_INSTALLED == 0.0.0 ]]; then
	echo "### could not fetch installed git version"

	read -e -p "# is this a fresh git installation? [y/N]: " -i "n" freshinstall
	if [[ ! $freshinstall =~ ^[Yy]*$ ]]; then
		echo "# installation aborted because local git version was not detectable"
		exit 1
	fi
fi

if [[ $GITVER == $GITVER_INSTALLED ]]; then
	echo "### git already up-to-date ($GITVER_INSTALLED)"
	exit 0
elif [[ $GITVER == 0.0.0 ]]; then
	echo "### could not fetch latest stable git version automatically"
	promptForVersion
else

	echo "### detected latest stable git release: $GITVER (currently installed: $GITVER_INSTALLED)"
	read -e -p "# proceed using this version? [Y/n]: " -i "y" proceed

	if [[ ! $proceed =~ ^[Yy]*$ ]]; then
		promptForVersion
	fi

fi

SRCARCHIVENAME=git-$GITVER
SRCARCHIVEFULLNAME=$SRCARCHIVENAME.tar.gz
SRCARCHIVE=$DDIR/$SRCARCHIVEFULLNAME
SRCDIR=$DDIR/$SRCARCHIVENAME

DOWNLOAD_SRC=1

# check if source archive was already downloaded
if [[ -e "$SRCARCHIVE" ]]; then
	read -e -p "# already downloaded archive detected > reuse? [y/N]: " -i "n" reusearchive
	if [[ $reusearchive =~ ^[Yy]*$ ]]; then
		DOWNLOAD_SRC=0
	fi
fi

if [[ DOWNLOAD_SRC -eq 1 ]]; then
	if [[ -e $SRCARCHIVE ]]; then
		rm -f $SRCARCHIVE
	fi

	echo "### downloading latest git source"
	# --progress=bar:force 2>&1 | tail -f -n +6
	if ! wget -P $DDIR --no-check-certificate "$DURL/$SRCARCHIVEFULLNAME"; then
		echo "# error downloading archive '$DURL/$SRCARCHIVEFULLNAME'"
		exit 1
	fi
fi

if [[ -e $SRCDIR ]]; then
	rm -rf $SRCDIR
fi

echo "### unpacking source"

if ! tar xzf "$SRCARCHIVE" -C $DDIR; then
	echo "# error during unpacking"
	exit 1
fi

echo "### configure installation"
cd "$SRCDIR"
./configure --prefix=$IPREF --with-ssl --without-tcltk

make

echo "### install"
make install

cd "$CWD"

if [[ $(getInstalledGitVersion) != $GITVER ]]; then
	echo "# installed version mismatch > something went wrong"
	exit 1
fi

echo "### post install cleanup"
rm $SRCARCHIVE
rm -rf $SRCDIR

echo "### finished successful"
exit 0
